const express = require("express");
const exphbs = require('express-handlebars');
const path = require('path');
const MongoClient = require("mongodb").MongoClient;
const objectId = require("mongodb").ObjectID;

const app = express();
const jsonParser = express.json();

const mongoClient = new MongoClient("mongodb://localhost:27017/", { useNewUrlParser: true });

let dbClient;

app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts')
}));
app.set('view engine', '.hbs');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(__dirname + "/public"));

mongoClient.connect(function(err, client){
    if(err) return console.log(err);
    dbClient = client;
    app.locals.collection = client.db("newsdb").collection("news");
    app.listen(3000, function(){
        console.log("Сервер ожидает подключения...");
    });
});

app.get("/news_list", function(req, res){
    const collection = req.app.locals.collection;
    collection.find({}).toArray(function(err, news){
        if(err) return console.log(err);
        res.send(news)
    });

});
app.get("/news_list/:id", function(req, res){

    const id = new objectId(req.params.id);
    const collection = req.app.locals.collection;
    collection.findOne({_id: id}, function(err, article){

        if(err) return console.log(err);
        res.send(article)
    });
});

app.get("/news", function(req, res){
    const collection = req.app.locals.collection;
    collection.find({}).toArray(function(err, news){
        if(err) return console.log(err);
        res.render('news.hbs', {
            news
        })
    });

});
app.get("/news/:id", function(req, res){

    const id = new objectId(req.params.id);
    const collection = req.app.locals.collection;
    collection.findOne({_id: id}, function(err, article){

        if(err) return console.log(err);
        res.render('news_detail.hbs', {
            article
        })
    });
});

app.post("/news", jsonParser, function (req, res) {

    if(!req.body) return res.sendStatus(400);

    const articleTitle = req.body.title;
    const articleDesc = req.body.description;
    const articleData = req.body.data;
    const article = {title: articleTitle, description: articleDesc, data: articleData};

    const collection = req.app.locals.collection;
    collection.insertOne(article, function(err, result){

        if(err) return console.log(err);
        res.send(article);
    });
});

app.delete("/news/:id", function(req, res){

    const id = new objectId(req.params.id);
    const collection = req.app.locals.collection;
    collection.findOneAndDelete({_id: id}, function(err, result){

        if(err) return console.log(err);
        let article = result.value;
        res.send(article);
    });
});

app.put("/news", jsonParser, function(req, res){

    if(!req.body) return res.sendStatus(400);
    const id = new objectId(req.body.id);
    const articleTitle = req.body.title;
    const articleDesc = req.body.description;
    const articleData = req.body.data;

    const collection = req.app.locals.collection;
    collection.findOneAndUpdate({_id: id}, { $set: {description: articleDesc, title: articleTitle, data: articleData}},
        {returnOriginal: false },function(err, result){

            if(err) return console.log(err);
            const article = result.value;
            res.send(article);
        });
});

// прослушиваем прерывание работы программы (ctrl-c)
process.on("SIGINT", () => {
    dbClient.close();
    process.exit();
});